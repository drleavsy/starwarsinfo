
pipeline {
  agent {
    node {
      label 'mac_agent'
      customWorkspace "workspace/$env.BUILD_NUMBER"
    }
  }
  environment {
    LANG = "en_US.UTF-8"
    PATH = "$HOME/.rbenv/shims:$PATH:/usr/local/bin:/usr/local/rvm/gems/ruby-3.0.0/bin/bundle:/home/ec2-user/bin"
    REPORT_DIR = "build/reports"
    LOG_DIR = ""
    LOG_PATH = ""
  }
  parameters {
    string(
      defaultValue: 'master',
      description: 'BRANCH',
      name: 'BRANCH' )
    string(
      defaultValue: '',
      description: 'ENV',
      name: 'ENV' )
    string(
      defaultValue: 'StarWarsInfoUITests',
      description: 'Project target in which to be run',
      name: 'TEST_TARGET'
    )
    string(
      defaultValue: '',
      description: '''For runnnig all suite please leave as default.
For running specific suite please run "/testClass/testFunc"''',
      name: 'TEST_SUITE'
    )
    string(
      name: 'TEST_LOCALE',
      defaultValue: "EnglishLocale",
      description: 'Set the Locale you want to run'
    )
    choice(
      name: 'IOS_VERSION',
      choices: "14.1", // should be kept identical to UNIT_TEST_IOS_VERSION
      description: 'Set IOS version'
    )
    choice(
      name: 'IPHONE_MODEL',
      choices: "12 Pro\n12\n12 mini", // should be kept identical to UNIT_TEST_IOS_DEVICE
      description: 'Set IPhone model'
    )
  }
  stages {
    stage("Git clone"){
      steps{
        script{
          git branch: "${params.BRANCH}", credentialsId: '93955a29-709f-4304-8bc3-eb30e12c5512', url: "git@bitbucket.org:drleavsy/starwarsinfo.git"
          //checkout([$class: �GitSCM�, branches: [[name: "${params.BRANCH}"]], userRemoteConfigs: [[url: "git@bitbucket.org:drleavsy/swiftstarwars.git"]]])
          sh "ls -ahl $HOME/workspace/$env.BUILD_NUMBER"
        }
      }
    }
    stage('Install Dependencies') {
      steps {
        script {
          //sh 'which bundle'
          sh 'echo "asd123" | sudo -S gem install bundler'
          sh 'echo "asd123" | sudo -S bundle update'
          sh 'bundle install'
          //sh 'bundle update || true' // Allows falling back to what was just installed, likely from the cache
        }
      }
    }
    stage("Tests") {
      steps {
        script {
          sh "xcrun xctrace list devices"
          sh "rm -rf ~/Library/Caches/org.swift.swiftpm"
		  sh "rm -rf ~/Library/Developer/Xcode/DerivedData"
		  
          withEnv([
            "UI_TEST_TARGET=${params.TEST_TARGET}",
            "UI_TEST_SUITE=${params.TEST_SUITE}",
            "UI_TEST_TESTPLAN=${params.TEST_PLAN}",
            "UI_TEST_LOCALE=${params.TEST_LOCALE}",
            "UI_TEST_IOS_VERSION=${params.IOS_VERSION}", 
            "UI_TEST_DEVICE=iPhone ${params.IPHONE_MODEL}", 
          ]) 
          {
            sh "bundle exec fastlane ui_test --env ${params.ENV}"
          }
        }
      }
    }
  }
  post {
    always {
      script {
          try {
            sh """log_dir="\$(find \$HOME/Library/Developer/Xcode/DerivedData/ -type d -name Test)"
                ls -ahl "\$log_dir"
                log_path="\$(find \$log_dir -name '*StarWarsInfo*.xcresult')"
                curl -O -L -s https://github.com/eroshenkoam/xcresults/releases/latest/download/xcresults
                chmod +x xcresults
                ls -ahl .
                ./xcresults export "\$log_path" "\$REPORT_DIR/allure"
                ls -ahl "\$REPORT_DIR/allure"
                """
          }
        catch (err) {
          err.getMessage()
          echo "Continue after error was detected."
        }
        archiveArtifacts "${REPORT_DIR}/*.*"
        archiveArtifacts "${REPORT_DIR}/**/*.*"
        junit skipPublishingChecks: true, allowEmptyResults: true, testResults: "**/report.junit" 
        allure includeProperties: false, jdk: '', results: [[path: "$REPORT_DIR/allure"]]
        //sh 'ls -ahl $REPORT_DIR'
        currentBuild.result = currentBuild.result ?: 'SUCCESS'
        //sh 'rm -rf ~/Library/Developer/Xcode/DerivedData/* ~/Library/Developer/Xcode/Archives/*'
        cleanWs()
      }
    }
  }
}